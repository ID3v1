#!/usr/bin/env python
'''
ID3v1

ID3 version 1 module, allows you to retrieve / edit the ID3v1 tag
on mp3 files.
'''

# ID3v1 Python Library
# Copyright (C) 2008  Alexander Borgerth <alex.borgert@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This is just a rough draft, it is a half working version, i have yet
# to implement writing abilitiy to the ID3 class.
# Most(if not all of this) will be refactored later.

import os
from struct import pack, unpack

class ID3InvalidTagError(Exception):
    '''
    Invalid Tag Error
    
    Is used when some data is invalid(retrieved), from the ID3 in
    a file.
    '''
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return self.msg

class ID3(object):
    '''
    ID3
    
    Is handling all reading / writing from / to the mp3 file.
    '''
    genres = ['Blues', 'Classic Rock', 'Country', 'Dance', 'Disco', 'Funk',
    'Grunge', 'Hip-Hop', 'Jazz', 'Metal', 'New Age', 'Oldies', 'Other',
    'Pop', 'R&B', 'Rap', 'Reggae', 'Rock', 'Techno', 'Industrial',
    'Alternative', 'Ska', 'Death Metal', 'Pranks', 'Soundtrack', 'Euro-Techno',
    'Ambient', 'Trip-Hop', 'Vocal', 'Jazz+Funk', 'Fusion', 'Trance',
    'Classical', 'Instrumental', 'Acid', 'House', 'Game', 'Sound Clip',
    'Gospel', 'Noise', 'AlternRock', 'Bass', 'Soul', 'Punk', 'Space',
    'Meditative', 'Instrumental Pop', 'Instrumental Rock', 'Ethnic', 'Gothic',
    'Darkwave', 'Techno-Industrial', 'Electronic', 'Pop-Folk', 'Eurodance',
    'Dream', 'Southern Rock', 'Comedy', 'Cult', 'Gangsta', 'Top 40',
    'Christian Rap', 'Pop/Funk', 'Jungle', 'Native American', 'Cabaret',
    'New Wave', 'Psychadelic', 'Rave', 'Showtunes', 'Trailer', 'Lo-Fi',
    'Tribal', 'Acid Punk', 'Acid Jazz', 'Polka', 'Retro', 'Musical',
    'Rock & Roll', 'Hard Rock']

    def __init__(self, filename):
        '''
        Initialize the data.
        
        Initialize all the data we will be using throughout
        the class.
        '''
        self.__TAG_SIZE = 128
        self.song = None
        self.artist = None
        self.album = None
        self.year = None
        self.comment = None
        self.genre = None
        
        self.header_changed = False
        
        if len(filename) <= 0:
            raise ValueError, 'filename is empty!'
        self.filename = filename
        # re-open with `r+b' only if we need to actually write any data!
        try:
            self.file = open(self.filename, 'rb')
        except IOError, value:
            print 'Couldn\'t open file: `%s\''
    
    def __valid_genre(self, genre_nr):
        '''
        Valid Genre
        
        Check to see if the ID3v1 genre, actually is a valid genre.
        '''
        if genre_nr > 0 and genre_nr < len(self.genres):
            return True
        return False
    
    def __has_tag(self):
        '''
        Check for tag.
        
        Check if the loaded file has an ID3v1 tag.
        '''
        pos = self.file.tell()
        if os.stat(self.filename).st_size < self.__TAG_SIZE:
            raise ValueError, 'File size < %db!' % (self.__TAG_SIZE)
        self.file.seek(-128, os.SEEK_END)
        if self.file.read(3) != 'TAG':
            return False
        self.file.seek(pos, os.SEEK_SET)
        return True
    
    def __pad_data(self, str, size=30):
        '''
        Pad data.
        
        Pads the string with '\0', if size is less then `size'. Also
        if the len(str) is higher then `size', truncate it!
        '''
        if len(str) <= 0:
            raise ValueError, '`str\' is empty!'
        if size <= 0:
            raise ValueError, '`size\' <= 0!'
        str = str[:size] # truncate to big strings!
        return str + ('\0' * (size - len(str)))
    
    def read(self):
        '''
        Read the ID3 tag from the file.
        
        Reads the ID3 tag from the file, and fill our data!
        '''
        if not self.__has_tag():
            raise ID3InvalidTagError, 'No ID3 tag found in file!'
        self.file.seek(-125, os.SEEK_END)
        raw = self.file.read(125)
        (self.song, self.artist, self.album, self.year, self.comment,\
        self.genre) = unpack('>30s30s30s4s30sB', raw)
    
    def __str__(self): # Used for debugging right now!
        data = 'Artist: %s\nAlbum: %s\nSong: %s\n' % (self.artist, self.album,\
        self.song)
        data = data + 'Year: %s\nGenre: %s\nComment: %s' % (self.year,\
        self.genres[self.genre], self.comment)
        return data
    
    def __setattr__(self, name, value):
        if name in ['artist', 'album', 'song', 'year', 'genre', 'comment']:
            self.__dict__['header_changed'] = True
            if name == 'genre':
                if self.__valid_genre(value):
                    self.__dict__[name] = value
                else:
                    self.__dict__[name] = 0
            else:
                self.__dict__[name] = value
        self.__dict__[name] = value
    
    def __validate_data(self):
        if len(self.song) > 0 and len(self.artist) > 0 and\
        len(self.album) > 0 and len(self.year) > 0 and\
        len(self.comment) > 0:
            if self.__valid_genre(self.genre):
                return True
        return false
    
    def write(self): # FIXME: broken!
        if self.header_changed:
            self.song = self.__pad_data(self.song, 30)
            self.artist = self.__pad_data(self.artist, 30)
            self.album = self.__pad_data(self.album, 30)
            self.year = self.__pad_data(self.year, 4)
            self.comment = self.__pad_data(self.comment, 30)
            self.genre = chr(self.genre)
        if self.__has_tag():
            try:
                self.file.close()
                self.file = open(self.filename, 'r+b')
            except IOError, value:
                print 'Couldn\'t open file(r+b): %s' % (value)
            self.file.seek(-125, os.SEEK_END)
            self.file.write(self.song)
            self.file.write(self.artist)
            self.file.write(self.album)
            self.file.write(self.year)
            self.file.write(self.comment)
            self.file.write(self.genre)
        else:
            # Implement the ability to add ID3 header, to headerless Mp3s
            pass
